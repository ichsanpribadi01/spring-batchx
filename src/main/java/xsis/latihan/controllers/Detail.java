package xsis.latihan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/detail")
public class Detail {
	@GetMapping(value = "detail")
	public ModelAndView detail() {
		ModelAndView view = new ModelAndView("/detail");
		return view;
	}

}
