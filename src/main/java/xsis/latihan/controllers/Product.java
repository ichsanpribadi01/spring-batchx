package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.VariantModel;
import xsis.latihan.models.ProductModel;
import xsis.latihan.repositories.VariantRepo;
import xsis.latihan.repositories.ProductRepo;

@Controller
@RequestMapping(value="/product/")
public class Product {
	@Autowired
	private ProductRepo productrepo;
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/product/index");
		List<ProductModel> product = this.productrepo.findAll();
		view.addObject("product", product);
		return view;
	}
	
	@Autowired
	private VariantRepo variantrepo;
	@GetMapping(value = "form")
	public ModelAndView from() {
		ModelAndView view = new ModelAndView("/product/form");
		ProductModel productmodel = new ProductModel();
		List<VariantModel> variant = this.variantrepo.findAll();
		view.addObject("variant", variant);
		view.addObject("product", productmodel);
		return view;
		
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute ProductModel productmodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/product/form");
		}else {
			this.productrepo.save(productmodel);
			return new ModelAndView("redirect:/product/index");
		}
		
	}
	@GetMapping(value = "edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/product/form");
		ProductModel productmodel=this.productrepo.findById(id).orElse(null);
		List<VariantModel> variant = this.variantrepo.findAll();
		view.addObject("variant", variant);
		view.addObject("product", productmodel);
		return view;
		
	}
	@GetMapping(value = "delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/product/delete");
		ProductModel productmodel=this.productrepo.findById(id).orElse(null);
		List<VariantModel> variant = this.variantrepo.findAll();
		view.addObject("variant", variant);
		view.addObject("product", productmodel);
		return view;
		
	}
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute ProductModel productmodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/product/form");
		}else {
			this.productrepo.delete(productmodel);
			return new ModelAndView("redirect:/product/index");
		}
		
	}
	

}
