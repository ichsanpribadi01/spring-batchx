package xsis.latihan.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name="category")
public class CategoryModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_category;
	
	@Column(name = "name")
	private String Name;
	
	@OneToMany(mappedBy = "categorymodel", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonManagedReference
	@Column(nullable = true)
	private Set<VariantModel> variantmodel;
	
	
	

	public Long getId_category() {
		return id_category;
	}

	public void setId_category(Long id_category) {
		this.id_category = id_category;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public CategoryModel(String name) {
		this.Name = name;
	}

	public CategoryModel( String name, Set<VariantModel> variantmodel) {
		super();
		Name = name;
		this.variantmodel = variantmodel;
	}
	

	public CategoryModel() {
		super();
	}

	
	
	
}
