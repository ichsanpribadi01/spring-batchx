package xsis.latihan.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="variant")
public class VariantModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "variant_name")
	private String variant_name;
	
	@Column(name = "category_id")
	private Integer Category_id;
	
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name="category_id", insertable = false, updatable = false )
	private CategoryModel categorymodel;
	
	@OneToMany(mappedBy = "variantmodel", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonManagedReference
	@Column(nullable = true)
	private Set<ProductModel> productmodel;
	public VariantModel() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVariant_name() {
		return variant_name;
	}

	public void setVariant_name(String variant_name) {
		this.variant_name = variant_name;
	}

	public Integer getCategory_id() {
		return Category_id;
	}

	public void setCategory_id(Integer category_id) {
		Category_id = category_id;
	}

	public CategoryModel getCategorymodel() {
		return categorymodel;
	}

	public void setCategorymodel(CategoryModel categorymodel) {
		this.categorymodel = categorymodel;
	}
	

	public VariantModel(String variant_name, Integer category_id, CategoryModel categorymodel,
			Set<ProductModel> productmodel) {
		super();
		this.variant_name = variant_name;
		Category_id = category_id;
		this.categorymodel = categorymodel;
		this.productmodel = productmodel;
	}
	
	
	
	
	

}
