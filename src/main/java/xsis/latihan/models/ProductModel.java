package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="product")
public class ProductModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name")
	private String Name;
	
	@Column(name = "price")
	private double Price;
	
	@Column(name = "stock")
	private Integer Stock;
	
	@Column(name = "variant_id")
	private Integer variant_id;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name="variant_id", insertable = false, updatable = false )
	private VariantModel variantmodel;
	public ProductModel(){}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}
	public Integer getStock() {
		return Stock;
	}
	public void setStock(Integer stock) {
		Stock = stock;
	}
	public Integer getVariant_id() {
		return variant_id;
	}
	public void setVariant_id(Integer variant_id) {
		this.variant_id = variant_id;
	}
	public VariantModel getVariantmodel() {
		return variantmodel;
	}
	public void setVariantmodel(VariantModel variantmodel) {
		this.variantmodel = variantmodel;
	}
	public ProductModel(String name, double price, Integer stock, Integer variant_id, VariantModel variantmodel) {
		super();
		Name = name;
		Price = price;
		Stock = stock;
		this.variant_id = variant_id;
		this.variantmodel = variantmodel;
	}
	
	
	
	
	
	
	

}
