package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import xsis.latihan.models.VariantModel;
import xsis.latihan.repositories.VariantRepo;
import xsis.latihan.service.VariantService;

@Service
public class VariantServiceimpl  implements VariantService {
	@Autowired
	private VariantRepo variantrepo;
	
	@Override
	public List<VariantModel> findAllVariant() {
		// TODO Auto-generated method stub
		return variantrepo.findAll();
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		variantrepo.deleteById(id);
		
	}

	@Override
	public VariantModel save(VariantModel variant) {
		// TODO Auto-generated method stub
		return variantrepo.save(variant);
	}

}
