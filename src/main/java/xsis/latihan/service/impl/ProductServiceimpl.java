package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.models.ProductModel;
import xsis.latihan.repositories.ProductRepo;
import xsis.latihan.service.ProductService;

@Service
public class ProductServiceimpl implements ProductService {
	
	@Autowired
	private ProductRepo productrepo;
	
	@Override
	public List<ProductModel> findAllProduct() {
		// TODO Auto-generated method stub
		return productrepo.findAll();
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		productrepo.deleteById(id);
		
	}

	@Override
	public ProductModel save(ProductModel product) {
		// TODO Auto-generated method stub
		return productrepo.save(product);
	}

}
