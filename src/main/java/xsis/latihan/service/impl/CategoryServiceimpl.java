package xsis.latihan.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.service.CategoryService;

@Service
public class CategoryServiceimpl implements CategoryService {

	@Autowired
	private CategoryRepo categoryrepo;
	
	@Override
	public List<CategoryModel> findAllCategory() {
		// TODO Auto-generated method stub
		return categoryrepo.findAll();
	}

	@Override
	public CategoryModel save(CategoryModel category) {
		// TODO Auto-generated method stub
		return categoryrepo.save(category);
	}

	@Override
	public void delete(Long id_category) {
		// TODO Auto-generated method stub
		categoryrepo.deleteById(id_category);
		
	}

	@Override
	public Optional<CategoryModel> findById(Long id_category) {
		// TODO Auto-generated method stub
		return categoryrepo.findById(id_category);
	}

}
