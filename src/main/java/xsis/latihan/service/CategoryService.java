package xsis.latihan.service;

import java.util.List;
import java.util.Optional;

import xsis.latihan.models.CategoryModel;

public interface CategoryService {
	List<CategoryModel>findAllCategory();
	CategoryModel save(CategoryModel category);
	
	void delete(Long id_category);

	Optional<CategoryModel>  findById(Long id_category);

}
