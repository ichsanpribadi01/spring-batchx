package xsis.latihan.service;

import java.util.List;

import xsis.latihan.models.VariantModel;

public interface VariantService {
	List<VariantModel>findAllVariant();
	void delete(Long id);
	VariantModel save(VariantModel variant);
}
