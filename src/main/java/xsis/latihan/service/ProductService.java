package xsis.latihan.service;

import java.util.List;

import xsis.latihan.models.ProductModel;


public interface ProductService {
	
	List<ProductModel>findAllProduct();
	void delete(Long id);
	ProductModel save(ProductModel product);
}
