package xsis.latihan.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import xsis.latihan.models.ProductModel;
import xsis.latihan.service.ProductService;

@RestController
@RequestMapping(path="/api/product", produces="application/json")
@CrossOrigin(origins = "*")
public class ProductRestController {
	@Autowired
	private ProductService productservice;
	@GetMapping("/")
	public ResponseEntity<?> findAllProduct(){
		return new ResponseEntity<>(productservice.findAllProduct(), HttpStatus.OK);
	}
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void Deleteproduct(@PathVariable("id")Long id) {
		try {
			productservice.delete(id);
		}catch (EmptyResultDataAccessException e) {
			
		}
	}

	@PostMapping("/add")
	public ResponseEntity<?> saveProduct(@RequestBody ProductModel product){
		return new ResponseEntity<>(productservice.save(product), HttpStatus.OK);
	}
	@PutMapping("/put")
	public ResponseEntity<?> putProduct(@RequestBody ProductModel product){
		return new ResponseEntity<>(productservice.save(product), HttpStatus.OK);
	}

}
