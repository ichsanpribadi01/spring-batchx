package xsis.latihan.rest.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.service.CategoryService;

@RestController
@RequestMapping(path="/api/category", produces="application/json")
@CrossOrigin(origins = "*")
public class CategoryRestController {
	@Autowired
	private CategoryService categoryservice;
	@GetMapping("/")
	public ResponseEntity<?> findAllCategory(){
		return new ResponseEntity<>(categoryservice.findAllCategory(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findCategoryById(@PathVariable("id") Long id_catagory) {
		return new ResponseEntity<>(categoryservice.findById(id_catagory), HttpStatus.OK);
	}
	@PostMapping("/add")
	public ResponseEntity<?> saveCategory(@RequestBody CategoryModel category){
		return new ResponseEntity<>(categoryservice.save(category), HttpStatus.OK);
	}
	@PutMapping("/put")
	public ResponseEntity<?> putCategory(@RequestBody CategoryModel category){
		return new ResponseEntity<>(categoryservice.save(category), HttpStatus.OK);
	}
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void DeleteCategory(@PathVariable("id")Long id_catagory) {
		try {
			categoryservice.delete(id_catagory);
		} catch (EmptyResultDataAccessException e) {
			
		}
	}
	
	
}
