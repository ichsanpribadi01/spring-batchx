package xsis.latihan.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import xsis.latihan.models.VariantModel;
import xsis.latihan.service.VariantService;

@RestController
@RequestMapping(path="/api/variant", produces="application/json")
@CrossOrigin(origins = "*")
public class VariantRestController {
	@Autowired
	private VariantService variantservice;
	@GetMapping("/")
	public ResponseEntity<?> findAllVariant(){
		return new ResponseEntity<>(variantservice.findAllVariant(), HttpStatus.OK);
	}
	
	@DeleteMapping("delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	private void DeleteVariant(@PathVariable("id")Long id) {
		try {
			variantservice.delete(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
		// TODO Auto-generated method stub

	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveVariant(@RequestBody VariantModel variant){
		return new ResponseEntity<>(variantservice.save(variant), HttpStatus.OK);
	}
	@PutMapping("/put")
	public ResponseEntity<?> putVariant(@RequestBody VariantModel variant){
		return new ResponseEntity<>(variantservice.save(variant), HttpStatus.OK);
	}

}
